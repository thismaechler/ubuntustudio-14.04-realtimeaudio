Ubuntu Studio 14.04 Realtime Optimization
==========================================

Author: This Mächler
maechler@mm-computing.ch
2014-09-22

Inspiered by this article (thanks a lot, it's really very usefull to me!):
http://wiki.linuxaudio.org/wiki/system_configuration

Here my works to optimize Ubuntu Studio 14.04 to reach the lowest 
audio latencies possible are documented.

There is a script that applies optimizations ("optimize-realtime"):

- installs service "rtaudio-mode" that stops services that may cause xruns 
  and switches to  CPU frequency scaling governor "performance"
- removes some packages containing parts that may cause xruns
  
There is also a pre-compiled realtime-kernel (3.18.7-rt1_0_amd64) available, the use of it
lowered the minimal applicable latencies dramatically in my case! (see "JACK configuration")

Please read HOWTO for further information.
