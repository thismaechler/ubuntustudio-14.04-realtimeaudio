APPLY OPTIMIZATIONS
=====================

1.
---
Edit "optimize-realtime" to change package list to uninstall / install


2.
--- 
Edit files in ./cfg/ to meet your hardware/software (rtirq priorities, services to stop in rtaudio-mode)

To list configurable interrupts for rtirq, run 'cat /proc/interrupts'. 
The names used in RTIRQ_NAME_LIST must match a part or the whole name of the interrupt:
'snd' or 'hda_intel' will match 'snd_hda_intel' ; 'usb1' or 'usb' will match 'ehci_hcd:usb1'


3. 
---
Run
> sudo ./optimize-realtime

- removes and adds packages
- installs an init.d-script to switch to "rtaudio-mode" (stop services like networking), 
- copies the files in cfg/ to the system and 
- installs a System-V style init script "rtaudio-mode" for the default runlevels


4. 
---
disable (gk)sudo password request for you (optional):

Run as root:
> visudo -f /etc/sudoers.d/[your username]-allow-all

Add this line:
[your username] ALL=(ALL) NOPASSWD: ALL

this prevents you from always typing your password when switching rtaudio-mode on/off.


5. 
---
in qjackctl, you may set start script to "gksudo service rtaudio-mode start" to automatically
start "rtaudio-mode" when starting jack server,

or manualy start/stop service in the console: 
> sudo service rtaudio-mode start
> sudo service rtaudio-mode stop

to setup rtaudio-mode to be set by default, run:
> sudo update-rc.d rtaudio-mode defaults

you may want to create starters to start/stop rtaudio-mode if you need it:
starter command to start rtaudio-mode: "gksudo service rtaudio-mode start"
starter command to stop rtaudio-mode: "gksudo service rtaudio-mode stop"

NOTE: to use gksudo, you need too install the package "gksu":
apt-get install gksu


6. 
---
Install realtime-kernel. 
This is optional, but lowered minimal latency dramatically in my case. 
May be different for newer kernel versions. Also mind that your kernel will not be updated automatically if
you install the custom realtime-kernel. To be on the safe side, use the default lowlatency-kernel.

To install the provided kernel (3.18.7-rt1_0_amd64) run
> cd rt-kernel
> sudo dpkg -i linux-{headers,image}-3.18.7-rt1_0_amd64.deb

Carlo Capocasa also provides Compiled Realtime-Kernels for Ubuntu:
https://capocasa.net/realtime-kernels

You can also build your own kernel following instructions in
./rt-kernel/README

or/and

http://wiki.linuxaudio.org/wiki/system_configuration

have fun with your realtime-audio system!



Check your realtime configuration ("perl" interpreter has to be installed)
===========================================================================
cd realtimeconfigquickscan
./realTimeConfigQuickScan.pl



JACK configuration:
=====================

Focusrite USB audio realtime prio = 75 (below usb-audio IRQ)

I run jackd2 in "synchronous mode" (option -S), it appears to be more stable 
and with less latency.

For USB-interfaces:
http://wiki.linuxaudio.org/wiki/list_of_jack_frame_period_settings_ideal_for_usb_interface

Those Buffer Settings work fine with realtime-kernel (Focusrite USB Audio):
* Frames 64 / Periods 3 / 48kHz (4ms) [no xruns up to highest DSP loads]
* 32 / Periods 3 / 48kHz (2ms) [some xruns at very high DSP loads]

With the default low-latency kernel, this is the lowest stable setting:
* Frames 128 / Periods 3 / 48kHz (8ms)



Appliaction buffer size (in my case "Bitwig")
=============================================
As low as possible without increasing DSP load too much



My software/hardware setup:
===========================
This optimization was tested/applied on a Ubuntu Studio 14.04 amd64
jackd2 + Bitwig Studio

Intel(R) Core(TM) i3-2330M CPU @ 2.20GHz
4GB RAM
Focusrite Scarlett 2i4 USB
Intel HDA (internal audio)


> cat /proc/interrupts 

           CPU0       CPU1       CPU2       CPU3       
  0:         33          0          0          0   IO-APIC-edge      timer
  1:       2559          0          0          0   IO-APIC-edge      i8042
  8:          1          0          0          0   IO-APIC-edge      rtc0
  9:       1117          0          0          0   IO-APIC-fasteoi   acpi
 12:     129923          0          0          0   IO-APIC-edge      i8042
 16:       5039         47          0          0   IO-APIC-fasteoi   ehci_hcd:usb1
 17:       1154      24621          0          0   IO-APIC-fasteoi   ath9k
 23:        186         13          0          0   IO-APIC-fasteoi   ehci_hcd:usb2
 40:      26697      10127          0          0   PCI-MSI-edge      ahci
 41:         14          0          0          0   PCI-MSI-edge      mei_me
 42:        956          0          0          0   PCI-MSI-edge      i915
 43:        289          0          0          0   PCI-MSI-edge      snd_hda_intel
 44:          0          0          0          0   PCI-MSI-edge      eth0
NMI:          2          2          1          1   Non-maskable interrupts
LOC:      50776      54441      43201      41917   Local timer interrupts
SPU:          0          0          0          0   Spurious interrupts
PMI:          2          2          1          1   Performance monitoring interrupts
IWI:       2343       2267       1700       1463   IRQ work interrupts
RTR:          1          0          0          0   APIC ICR read retries
RES:       5469       5615       4782       5016   Rescheduling interrupts
CAL:        531        404        486        345   Function call interrupts
TLB:        423        641        501        312   TLB shootdowns
TRM:          0          0          0          0   Thermal event interrupts
THR:          0          0          0          0   Threshold APIC interrupts
MCE:          0          0          0          0   Machine check exceptions
MCP:          6          6          6          6   Machine check polls
ERR:          0
MIS:          0


> lsusb

Bus 002 Device 005: ID 1235:800a Novation EMS (Focusrite Scarlett 2i4)
Bus 002 Device 004: ID 0582:0033 Roland Corp. EDIROL PCR
Bus 002 Device 003: ID 046d:c52f Logitech, Inc. Unifying Receiver
Bus 002 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 001 Device 005: ID 058f:6366 Alcor Micro Corp. Multi Flash Reader
Bus 001 Device 004: ID 13d3:5710 IMC Networks UVC VGA Webcam
Bus 001 Device 003: ID 0cf3:3005 Atheros Communications, Inc. AR3011 Bluetooth
Bus 001 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub



REALTIME OPTIMIZATIONS
=======================

* CPU FREQUENCY SCALING
Switch to "performance" Realtime-Audio System Mode

* IRQ PRIO CONFIG
rtc (90)
usb-audio/midi-controller on USB port 2 (80)
internal audio (70)
keyboard (60)

* REMOVE PACKAGES
Remove unnecessary/problematic packages ("apt-xapian-index", zeitgeist, pulseaudio, bluetooth)






